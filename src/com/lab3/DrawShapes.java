package com.lab3;

public abstract class DrawShapes {

  public static void drawSquare() {
    for (int i = 0; i < 5; ++i) {
      for (int j = 0; j < 5; ++j) {
        System.out.print("  o");
      }
      System.out.println();
    }
    System.out.println();
  }

  public static void drawTriangle() {
    for (int i = 0; i < 5; ++i) {
      for (int j = 0; j < 5; ++j) {
        if (i >= j) {
          System.out.print("  o");
        } else {
          System.out.print("   ");
        }
      }
      System.out.println();
    }
    System.out.println();
  }
}
