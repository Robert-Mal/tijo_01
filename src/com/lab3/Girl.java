package com.lab3;

class Girl {
  private Flower flower;
  private short age;

  public Girl(short age) {
    this.age = age;
  }

  public Flower flower() {
    return flower;
  }

  public void receiveFlower(Flower flower) {
    this.flower = flower;
  }

  public short age() {
    return age;
  }

  public void setAge(short age) {
    this.age = age;
  }
}
