package com.lab3;

public class Main {

    public static void printFigure(Figure figure) {

        figure.print(5);
    }

    public static void main(String[] args) {

        short age = 33;
        Girl monicaBellucci = new Girl(age);

        monicaBellucci.receiveFlower(new Rose());
        monicaBellucci.setAge(age);
        monicaBellucci.flower();
        monicaBellucci.age();

        printFigure(new Square());
        printFigure(new Triangle());
        printFigure(new Diamond());
    }
}