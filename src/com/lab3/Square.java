package com.lab3;

public class Square implements Figure {

  public void print(int size) {
    for (int i = 0; i < size; ++i) {
      for (int j = 0; j < size; ++j) {
        System.out.print("  o");
      }
      System.out.println();
    }
    System.out.println();
  }
}
