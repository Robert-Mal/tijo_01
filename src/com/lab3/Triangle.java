package com.lab3;

public class Triangle implements Figure {

  public void print(int size) {
    for (int i = 0; i < size; ++i) {
      for (int j = 0; j < size; ++j) {
        if (i >= j) {
          System.out.print("  o");
        } else {
          System.out.print("   ");
        }
      }
      System.out.println();
    }
    System.out.println();
  }
}
